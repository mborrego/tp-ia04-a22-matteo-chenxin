package restserveragent

import (
	"TP1/comsoc"
	"TP1/types"
	"TP1/utility"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"
)

type RestServerAgent struct {
	sync.Mutex
	addr    string
	ballots map[string]types.Ballot
	rules   []string
}

func NewRestServerAgent(addr string) *RestServerAgent {
	ballots := make(map[string]types.Ballot)
	return &RestServerAgent{addr: addr, ballots: ballots, rules: []string{"dictatorship", "copeland", "ks", "majority", "majority2turns", "borda"}}
}

func (rsa *RestServerAgent) newBallot(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if r.Method != "POST" {
		w.WriteHeader(400)
		return
	}

	var newBallotPattern types.BallotPattern
	utility.DecodeRequest(r, &newBallotPattern)

	// If the rule is not implemented
	if !utility.Contains(rsa.rules, newBallotPattern.Rule) {
		w.WriteHeader(501)
		return
	}

	profile := make([][]int, len(newBallotPattern.VoterIds))
	var agentDoneId []string

	rsa.ballots[strconv.Itoa(len(rsa.ballots)+1)] = types.Ballot{Pattern: newBallotPattern, Profile: profile, AgentDoneId: agentDoneId}
	w.WriteHeader(201)

	resp := types.BallotId{BallotId: strconv.Itoa(len(rsa.ballots))}
	data, _ := json.Marshal(resp)
	w.Write([]byte(data))
}

func (rsa *RestServerAgent) vote(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if r.Method != "POST" {
		w.WriteHeader(400)
		return
	}

	var newVote types.Vote
	utility.DecodeRequest(r, &newVote)

	// If the requested ballot doesn't exist
	if rsa.ballots[newVote.VoteId].Pattern.Rule == "" {
		w.WriteHeader(501)
		return
	}

	// If the agent has already submitted a vote for this ballot
	if utility.Contains(rsa.ballots[newVote.VoteId].AgentDoneId, newVote.AgentId) {
		w.WriteHeader(403)
		return
	}

	// Searches for the next empty slot in profile
	i := 0
	for ; i < len(rsa.ballots[newVote.VoteId].Profile) && rsa.ballots[newVote.VoteId].Profile[i] != nil; i++ {
	}

	if i >= len(rsa.ballots[newVote.VoteId].Profile) {
		w.WriteHeader(400)
		return
	}

	rsa.ballots[newVote.VoteId].Profile[i] = newVote.Prefs

	updatedBallot := rsa.ballots[newVote.VoteId]
	updatedBallot.AgentDoneId = append(updatedBallot.AgentDoneId, newVote.AgentId)
	rsa.ballots[newVote.VoteId] = updatedBallot

	w.WriteHeader(200)
}

func (rsa *RestServerAgent) result(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	var ballotId types.BallotId
	utility.DecodeRequest(r, &ballotId)

	// If the requested ballot doesn't exist
	if rsa.ballots[ballotId.BallotId].Pattern.Rule == "" {
		w.WriteHeader(404)
		return
	}

	// If the ballot is still ongoing
	if rsa.ballots[ballotId.BallotId].Pattern.Deadline.After(time.Now()) && len(rsa.ballots[ballotId.BallotId].Pattern.VoterIds) != len(rsa.ballots[ballotId.BallotId].AgentDoneId) {
		w.WriteHeader(425)
		return
	}

	if rsa.ballots[ballotId.BallotId].Winner == 0 {
		var bestAlts []int

		// Removes empty preferences from profile
		updatedBallot1 := rsa.ballots[ballotId.BallotId]
		updatedBallot1.Profile = utility.RmvMissingPrefs(rsa.ballots[ballotId.BallotId].Profile)
		rsa.ballots[ballotId.BallotId] = updatedBallot1

		switch rsa.ballots[ballotId.BallotId].Pattern.Rule {
		case "dictatorship":
			bestAlts, _ = comsoc.DictatorshipSCF(rsa.ballots[ballotId.BallotId].Profile)
		case "copeland":
			bestAlts, _ = comsoc.CopelandSCF(rsa.ballots[ballotId.BallotId].Profile)
		case "ks":
			bestAlts, _ = comsoc.KSSCF(rsa.ballots[ballotId.BallotId].Profile)
		case "majority":
			bestAlts, _ = comsoc.MajoritySCF(rsa.ballots[ballotId.BallotId].Profile)
		case "majority2turns":
			bestAlts, _ = comsoc.Majority2TurnsfSCF(rsa.ballots[ballotId.BallotId].Profile)
		case "borda":
			bestAlts, _ = comsoc.BordaSCF(rsa.ballots[ballotId.BallotId].Profile)
		default:
			log.Panicln("Can not determine the result of a ballot with an unimplemented rule")
			return
		}
		updatedBallot2 := rsa.ballots[ballotId.BallotId]
		updatedBallot2.Winner, _ = comsoc.TieBreak(bestAlts)
		rsa.ballots[ballotId.BallotId] = updatedBallot2
	}

	resp := types.Result{Winner: rsa.ballots[ballotId.BallotId].Winner, Ranking: rsa.ballots[ballotId.BallotId].Ranking}
	data, _ := json.Marshal(resp)
	w.WriteHeader(200)
	w.Write([]byte(data))
}

func (rsa *RestServerAgent) info(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	if r.Method != "POST" {
		w.WriteHeader(400)
		return
	}

	var ballotId types.BallotId
	utility.DecodeRequest(r, &ballotId)

	// If the requested ballot doesn't exist
	if rsa.ballots[ballotId.BallotId].Pattern.Rule == "" {
		w.WriteHeader(501)
		return
	}

	resp := rsa.ballots[ballotId.BallotId]
	data, _ := json.Marshal(resp)

	w.WriteHeader(200)
	w.Write([]byte(data))
}

func (rsa *RestServerAgent) Start() {

	mux := http.NewServeMux()

	serv := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   5 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	mux.HandleFunc("/new_ballot", rsa.newBallot)
	mux.HandleFunc("/vote", rsa.vote)
	mux.HandleFunc("/result", rsa.result)
	mux.HandleFunc("/info", rsa.info)
	mux.HandleFunc("/shutdown", func(w http.ResponseWriter, r *http.Request) {
		serv.Shutdown(context.Background())
	})

	log.Println("Launching the server...")
	go log.Fatal(serv.ListenAndServe())
}
