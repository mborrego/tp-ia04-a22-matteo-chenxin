# Lien gitlab

[https://gitlab.utc.fr/mborrego/tp-ia04-a22-matteo-chenxin](https://gitlab.utc.fr/mborrego/tp-ia04-a22-matteo-chenxin)

# Comment lancer l'application

go run cmd/launchserver.go pour lancer le serveur  
go run cmd/launchclient.go pour lancer le client


# Utiliser le serveur

Le serveur n'a pas besoin de manipulation particulière.  
Une fois lancé, il peut être clôturé en appuyant sur CTRL+C ou en demandant la clôture du serveur depuis le client.

# Utiliser le client

Le client permet d'effectuer tous les tests de requêtes avec le serveur.

## Au lancement

Après avoir lancé le client, il va demander à l'utilisateur de rentrer son identifiant :  
*"How would you like to call the client?"*  
Cet identifiant sert à différencier les différents agents votants.  

Exemple d'identifiant valide : *"test1"*

## Menu principal

Le menu principal consiste en sept options, chacune accessible en tapant le chiffre correspondant, suivant de la touche ENTRÉE :  
- [0] Créer un nouveau bulletin de vote
- [1] Voter pour un bulletin de vote
- [2] Voir les résultats d'un bulletin de vote
- [3] Voir toutes les informations d'un bulletin de vote
- [4] Changer l'identifiant du client
- [5] Fermer le serveur
- [6] Fermer le client

## Créer un nouveau bulletin de vote

### Choix de la règle de vote

Le client demande à l'utilisateur de taper le nom de la règle de vote que suivra le bulletin de vote en minuscules et d'appuyer sur la touche ENTRÉE :  
*"Which rule will this ballot follow? ("dictatorship", "copeland", "majority", "majority2turns", "ks", "borda", ...)"*  
  
Attention : seulement "dictatorship", "copeland", "majority", "majority2turns", "ks" et "borda" sont actuellement implémentées.

### Date de fin du bulletin de vote

Le client demande à l'utilisateur de taper la date de fin du bulletin de vote au format DD/MM/YYYY puis d'appuyer sur la touche ENTRÉE :  
*"When will this ballot end? (DD/MM/YYYY)"*  
  
Exemple de date valide : *"31/12/2022"*

### Heure de fin du bulletin de vote

Le client demande à l'utilisateur de taper l'heure de fin du bulletin de vote au format HH:MM:SS puis d'appuyer sur la touche ENTRÉE :  
*"At what time? (HH:MM:SS)"*  
  
Exemple d'heure valide : *"23:59:59"*

### Nombre d'agents votants

Le client demande à l'utilisateur de taper le nombre d'agents votants qui peuvent participer au bulletin de vote puis d'appuyer sur la touche ENTRÉE :  
*"How many voters are there?"*

### Identifiants des votants

Le client demande à l'utilisateur de taper successivement les identifiants des agents votants qui peuvent participer au bulletin de vote puis d'appuyer sur la touche ENTRÉE après chaque identifiant :  
*"Who is voter n°1?"*  
  
Exemple d'identifiant valide : *"test1"*

### Nombre d'alternatives

Le client demande à l'utilisateur de taper le nombre d'alternatives pour ce bulletin de vote puis d'appuyer sur la touche ENTRÉE :  
*"How many alternatives are there?"*

### Résultat de la requête

Le client peut ensuite afficher quatre messages différents selon ce que lui a renvoyé le serveur :  
- *"Ballot created with the ID: X"* (X étant l'identifiant du bulletin de vote choisi par le serveur) si le bulletin de vote a bien été ajouté,
- *"Something went wrong with the request"* si la requête n'est pas de type POST,
- *"This ballot's rule is not implemented yet"* si le bulletin suit une règle de vote qui n'est pas implémentée par le serveur,
- *"Unknown status code returned by server"* si le serveur renvoit un code de statut que le client ne connait pas.

## Voter pour un bulletin de vote

### Choix du bulletin de vote pour lequel voter

Le client demande à l'utilisateur de taper l'identifiant du bulletin de vote pour lequel il souhaite voter puis d'appuyer sur la touche ENTRÉE :  
*"Which ballot would you like to vote in? (Enter its ID)"*

### Choix du nombre d'alternatives pour lesquelles voter

Le client demande à l'utilisateur de taper le nombre d'alternatives pour lesquelles il souhaite voter pour ce bulletin de vote puis d'appuyer sur la touche ENTRÉE :  
*"How many alternatives would you like to vote for?"*

### Numéros de ses préférences

Le client demande à l'utilisateur de taper successivement les numéros des alternatives pour lesquelles il souhaite voter par ordre de préférence puis d'appuyer sur la touche ENTRÉE après chaque numéro :  
*"Who is your choice n°1?"*

### Choix du nombre d'options

Le client demande à l'utilisateur de taper le nombre d'options qu'ils souhaitent ajouter à son vote puis d'appuyer sur la touche ENTRÉE :  
*"How many options does your vote have?"*

### Options de vote

Le client demande à l'utilisateur de taper successivement les options de vote qu'il souhaite renseigner puis d'appuyer sur la touche ENTRÉE après chaque numéro :  
*"What is your option n°1?"*

### Résultat de la requête

Le client peut ensuite afficher six messages différents selon ce que lui a renvoyé le serveur :  
- *"Vote successfully added to the ballot"* si le vote a bien été pris en compte,
- *"Something went wrong with the request"* si la requête n'est pas de type POST,
- *"You have already voted for this ballot"* si l'agent votant a déjà voté pour ce bulletin de vote,
- *"This ballot doesn't exist"* si le serveur n'a pas trouvé de bulletin de vote correspondant à l'identifiant renseigné,
- *"This ballot has already reached its deadline"* si le bulletin de vote en question a passé sa date de clôture,
- *"Unknown status code returned by server"* si le serveur renvoit un code de statut que le client ne connait pas.

## Voir les résultats d'un bulletin de vote

### Choix du bulletin de vote pour lequel voir les résultats

Le client demande à l'utilisateur de taper l'identifiant du bulletin de vote pour lequel il souhaite voir les résultats puis d'appuyer sur la touche ENTRÉE :  
*"Which ballot would you like to see the result of? (Enter its ID)"*

### Résultat de la requête

Le client peut ensuite afficher quatre messages différents selon ce que lui a renvoyé le serveur :  
- *"The winner of this ballot is: 1"* (1 étant le gagnant du bulletin de vote) si le bulletin existe et que, soit tout le monde a voté, soit sa date de clôture est passée,
- *"This ballot is still ongoing"* si le bulletin n'a pas encore passé sa date de clôture et que tous les agents votants prévus n'ont pas encore voté,
- *"Could not find the results of this ballot"* si le serveur n'a pas trouvé de bulletin de vote correspondant à l'identifiant renseigné,
- *"Unknown status code returned by server"* si le serveur renvoit un code de statut que le client ne connait pas.

## Voir toutes les informations d'un bulletin de vote

### Choix du bulletin de vote pour lequel voir les informations

Le client demande à l'utilisateur de taper l'identifiant du bulletin de vote pour lequel il souhaite voir les informations complètes puis d'appuyer sur la touche ENTRÉE :  
*"Which ballot would you like to see? (Enter its ID)"*

### Résultat de la requête

Le client peut ensuite afficher quatre messages différents selon ce que lui a renvoyé le serveur :  
- [Afficher directement la structure types.Ballot du bulletin de vote] si le serveur a trouvé le bulletin de vote correspondant à l'identifiant renseigné,
- *"Something went wrong with the request"* si la requête n'est pas de type POST,
- *"This ballot doesn't exist"* si le serveur n'a pas trouvé de bulletin de vote correspondant à l'identifiant renseigné,
- *"Unknown status code returned by server"* si le serveur renvoit un code de statut que le client ne connait pas.

## Changer l'identifiant du client

Cette option permet au client de se faire passer pour différents agents votants, ce qui permet de réaliser n'importe quel test de vote.

### Nouvel identifiant

Le client affiche son identifiant actuel :  
*"Old ID: test1"*  
  
Puis demande à l'utilisateur de taper un nouvel identifiant puis d'appuyer sur la touche ENTRÉE :  
*"Enter your new ID:"*  
  
Exemple de nouvel identifiant valide : *"test2"*

### Résultat

Le client n'affiche aucun message car cette fonctionnalité ne nécessite aucune communication avec le seveur.

## Fermer le serveur

Cette option permet de fermer le serveur sans avoir à faire CTRL+C depuis sa console.

### Résultat

Le client peut afficher deux messages différents selon si le serveur lui a renvoyé un signal ou s'est fermé correctement :  
- *"Server successfully shut down"* si le serveur s'est bien clôturé et n'a rien renvoyé au client,
- *"There was an error while trying to shut down the server"* si le serveur n'a pas pu se clôturer.

## Fermer le client

Cette option permet de fermer le client sans avoir à faire CTRL+C depuis sa console.

### Résultat

Le client affiche le message :  
*"Shutting down the client..."*  
  
Puis il s'éteint.