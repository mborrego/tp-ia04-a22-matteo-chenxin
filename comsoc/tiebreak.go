package comsoc

import (
	"errors"
	"math/rand"
	"time"
)

// func TieBreak(bestAlts []int) (alternative int) {
// 	s := rand.NewSource(time.Now().UnixNano())
// 	r := rand.New(s)
// 	return bestAlts[r.Intn(len(bestAlts))]
// }

func TieBreak(bestAlts []int) (alternative int, err error) {
	if len(bestAlts) == 0 {
		return alternative, errors.New("no best alternative")
	}

	s := rand.NewSource(time.Now().UnixNano())
	r := rand.New(s)
	return bestAlts[r.Intn(len(bestAlts))], err
}

func TieBreakFactory(hierarchyAlts []int) func(bestAlts []int) (alternative int, err error) {
	return func(bestAlts []int) (alternative int, err error) {
		if len(bestAlts) == 0 {
			return 0, errors.New("no best alternative")
		}
		min := len(hierarchyAlts) - 1
		for _, v := range bestAlts {
			for i, v2 := range hierarchyAlts {
				if v == v2 && i < min {
					min = i
				}
			}
		}
		return hierarchyAlts[min], nil
	}
}
