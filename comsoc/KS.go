package comsoc

import (
	"TP1/utility"
)

// comparaison de Condorcet entre deux alternatives
func AlternativeCompareKS(prefs [][]int, alt int, oppo int) int {
	var vote_yes int
	var vote_no int
	for i := 0; i < len(prefs); i++ {
		res := utility.IsPref(alt, oppo, prefs[i])
		if res {
			vote_yes += 1
		} else {
			vote_no += 1
		}
	}
	if vote_yes > vote_no {
		return 1
	} else if vote_yes < vote_no {
		return 0
	} else {
		return 0
	}
}

func KSSWF(prefs [][]int) (map[int]int, error) {
	ct := make(map[int]int)
	var alts_slice []int
	for i := 0; i < len(prefs[0]); i++ {
		alts_slice = append(alts_slice, prefs[0][i])
	}
	for i := 0; i < len(alts_slice); i++ {
		point := make([]int, 0)
		alt := int(alts_slice[i])
		for j := 0; j < len(alts_slice); j++ {
			oppo := int(alts_slice[j])
			if alt != oppo {
				point[j] += AlternativeCompareCopeland(prefs, alt, oppo)
			}
		}
		ct[alt] = Min(point)
	}
	return ct, nil
}

func KSSCF(prefs [][]int) (bestAlts []int, err error) {
	ct, err_swf := CopelandSWF(prefs)
	best_alts := utility.MaxCount(ct)
	return best_alts, err_swf
}

func Min(x []int) int {
	min := x[0]
	for _, v := range x {
		if v < min {
			min = v
		}
	}
	return min
}
