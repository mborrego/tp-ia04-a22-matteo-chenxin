package comsoc

import (
	"TP1/utility"
	"errors"
	"sort"
)

func Majority2Turns(p [][]int) (count map[int]int, err error) {
	// same as Majority
	count, err = MajoritySWF(p)
	return
}

func Majority2TurnsfSCF(p [][]int) (bestAlts []int, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	//first tour
	first_count, first_alt, second_alt := FirstTurn(count)
	half_ballot := len(p) / 2
	//winner winner
	if first_count > half_ballot {
		bestAlts = append(bestAlts, first_alt)
		return
	}
	// second tour
	bestAlts, err = SecondTurn(first_alt, second_alt, p)

	return
}

// find the most votes, the alt who has the most votes and the alt who has the second votes
func FirstTurn(count map[int]int) (first_count int, first_alt int, second_alt int) {
	asList := sortCount(count)
	first_count = asList[0].score
	first_alt = asList[0].alt
	second_alt = asList[1].alt
	return
}

func SecondTurn(first_alt int, second_alt int, p [][]int) (bestAlts []int, err error) {
	first_count := 0
	second_count := 0
	for _, pref := range p {
		rank1 := utility.Rank(first_alt, pref)
		rank2 := utility.Rank(second_alt, pref)
		if rank1 == -1 || rank2 == -1 {
			return nil, errors.New("rank error")
		}
		if rank1 < rank2 {
			first_count++
		} else if rank1 > rank2 {
			second_count++
		}
	}
	if first_count == 0 && second_count == 0 {
		return nil, errors.New("rank counting error")
	}
	if first_count > second_count {
		bestAlts = append(bestAlts, first_alt)
	} else if first_count < second_count {
		bestAlts = append(bestAlts, second_alt)
	} else {
		bestAlts = append(bestAlts, first_alt)
		bestAlts = append(bestAlts, second_alt)
	}
	return bestAlts, nil
}

type alt_score struct {
	alt   int
	score int
}
type alt_score_list []alt_score

func (pair alt_score_list) Len() int {
	return len(pair)
}
func (pair alt_score_list) Less(i, j int) bool {
	return pair[i].score < pair[j].score
}
func (pair alt_score_list) Swap(i, j int) {
	pair[i], pair[j] = pair[j], pair[i]
}

func sortCount(count map[int]int) (asList alt_score_list) {
	asList = make(alt_score_list, 0)
	for alt, score := range count {
		asList = append(asList, alt_score{alt, score})

	}
	sort.Sort(sort.Reverse(asList))
	return
}
