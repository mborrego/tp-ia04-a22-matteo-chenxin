package comsoc

import (
	"TP1/utility"
)

func MajoritySWF(p [][]int) (count map[int]int, err error) {
	prof_err := utility.CheckProfile(p)
	if prof_err != nil {
		return nil, prof_err
	}

	count = make(map[int]int)

	for _, row := range p {
		count[row[0]]++
	}

	return count, nil
}

func MajoritySCF(p [][]int) (bestAlts []int, err error) {
	count, erreur := MajoritySWF(p)
	if erreur != nil {
		return nil, erreur
	}

	bestAlts = utility.MaxCount(count)

	return bestAlts, nil
}
