package comsoc

import (
	"TP1/utility"
	"fmt"
)

// The first voter is always right
func DictatorshipSWF(prefs [][]int) (map[int]int, error) {
	ct := make(map[int]int)
	var alts_slice []int

	for i := 0; i < len(prefs[0]); i++ {
		alts_slice = append(alts_slice, prefs[0][i])
	}

	ct[int(alts_slice[0])] = 1

	fmt.Println(ct)

	return ct, nil
}

func DictatorshipSCF(prefs [][]int) (bestAlts []int, err error) {
	ct, err_swf := DictatorshipSWF(prefs)
	best_alts := utility.MaxCount(ct)
	return best_alts, err_swf
}
