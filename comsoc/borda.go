package comsoc

import "TP1/utility"

func BordaSWF(p [][]int) (count map[int]int, err error) {
	prof_err := utility.CheckProfile(p)
	if prof_err != nil {
		return nil, prof_err
	}
	count = make(map[int]int)
	for _, pref := range p {
		length_pref := len(pref)
		for i, alt := range pref {
			count[alt] += length_pref - 1 - i
		}
	}
	return count, nil
}

func BordaSCF(p [][]int) (bestAlts []int, err error) {
	count, erreur := BordaSWF(p)
	if erreur != nil {
		return nil, erreur
	}
	bestAlts = utility.MaxCount(count)
	err = nil
	return
}
