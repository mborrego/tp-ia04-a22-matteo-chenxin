package restclientagent

import (
	"TP1/types"
	"TP1/utility"
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"
)

type RestClientAgent struct {
	id  string
	url string
}

func NewRestClientAgent(id string, url string) *RestClientAgent {
	return &RestClientAgent{id, url}
}

func mainMenu() (clCmd string) {
	time.Sleep(time.Second)
	fmt.Println("")
	fmt.Println("--------------------------------------------------")

	fmt.Println("What do you want to do?")
	fmt.Println("[0] Create a new ballot")
	fmt.Println("[1] Vote for a ballot")
	fmt.Println("[2] See the results of a ballot")
	fmt.Println("")
	fmt.Println("[3] See the general informations of a ballot")
	fmt.Println("[4] Change your ID")
	fmt.Println("[5] Shut the server down")
	fmt.Println("[6] Shut the client down")

	fmt.Scan(&clCmd)

	for !utility.Contains([]string{"0", "1", "2", "3", "4", "5", "6"}, clCmd) {
		fmt.Println("Unknown command")
		fmt.Scan(&clCmd)
	}

	return
}

func (rca *RestClientAgent) newBallot() {
	req := types.BallotPattern{}

	var deadlineDate string
	var deadlineTime string
	var nbVoters int

	fmt.Println("Which rule will this ballot follow? (\"dictatorship\", \"copeland\", \"majority\", \"majority2turns\", \"ks\", \"borda\", ...)")
	fmt.Scan(&req.Rule)

	fmt.Println("When will this ballot end? (DD/MM/YYYY)")
	fmt.Scan(&deadlineDate)
	fmt.Println("At what time? (HH:MM:SS)")
	fmt.Scan(&deadlineTime)
	req.Deadline, _ = time.ParseInLocation("02/01/2006 15:04:05", deadlineDate+" "+deadlineTime, time.Local)

	fmt.Println("How many voters are there?")
	fmt.Scan(&nbVoters)
	req.VoterIds = make([]string, nbVoters)
	for i := range req.VoterIds {
		fmt.Println("Who is voter n°" + strconv.Itoa(i+1) + "?")
		fmt.Scan(&req.VoterIds[i])
	}

	fmt.Println("How many alternatives are there?")
	fmt.Scan(&req.Alts)

	data, _ := json.Marshal(req)
	resp, _ := http.Post(rca.url+"/new_ballot", "application/json", bytes.NewBuffer(data))

	switch resp.StatusCode {
	case 400:
		log.Println("Something went wrong with the request")
	case 501:
		log.Println("This ballot's rule is not implemented yet")
	case 201:
		var newBallotId types.BallotId
		utility.DecodeResponse(resp, &newBallotId)
		log.Println("Ballot created with the ID: " + newBallotId.BallotId)
	default:
		log.Fatalln("Unknown status code returned by server")
	}
}

func (rca *RestClientAgent) vote() {
	req := types.Vote{AgentId: rca.id}

	fmt.Println("Which ballot would you like to vote in? (Enter its ID)")
	fmt.Scan(&req.VoteId)

	req.Prefs = make([]int, 0)

	var nbAlt int
	fmt.Println("How many alternatives would you like to vote for?")
	fmt.Scan(&nbAlt)

	var choice int
	for i := 0; i < nbAlt; i++ {
		fmt.Println("Who is your choice n°" + strconv.Itoa(i+1) + "?")
		fmt.Scan(&choice)
		req.Prefs = append(req.Prefs, choice)
	}

	var nbOptions int
	fmt.Println("How many options does your vote have?")
	fmt.Scan(&nbOptions)

	var option int
	for i := 0; i < nbOptions; i++ {
		fmt.Println("What is your option n°" + strconv.Itoa(i+1) + "?")
		fmt.Scan(&option)
		req.Options = append(req.Options, option)
	}

	data, _ := json.Marshal(req)

	resp, _ := http.Post(rca.url+"/vote", "application/json", bytes.NewBuffer(data))

	switch resp.StatusCode {
	case 200:
		log.Println("Vote successfully added to the ballot")
	case 400:
		log.Println("Something went wrong with the request")
	case 403:
		log.Println("You have already voted for this ballot")
	case 501:
		log.Println("This ballot doesn't exist")
	case 503:
		log.Println("This ballot has already reached its deadline")
	default:
		log.Fatalln("Unknown status code returned by server")
	}
}

func (rca *RestClientAgent) result() {
	var ballotId types.BallotId
	fmt.Println("Which ballot would you like to see the result of? (Enter its ID)")
	fmt.Scan(&ballotId.BallotId)

	data, _ := json.Marshal(ballotId)
	resp, _ := http.Post(rca.url+"/result", "application/json", bytes.NewBuffer(data))

	switch resp.StatusCode {
	case 200:
		var results types.Result
		utility.DecodeResponse(resp, &results)
		log.Println("The winner of this ballot is: " + strconv.Itoa(results.Winner))
		if len(results.Ranking) != 0 {
			log.Println("The final ranking is: ")
			log.Println(results.Ranking)
		}
	case 425:
		log.Println("This ballot is still ongoing")
	case 404:
		log.Println("Could not find the results of this ballot")
	default:
		log.Fatalln("Unknown status code returned by server")
	}
}

func (rca *RestClientAgent) info() {
	var ballotId types.BallotId

	fmt.Println("Which ballot would you like to see? (Enter its ID)")
	fmt.Scan(&ballotId.BallotId)

	data, _ := json.Marshal(ballotId)
	resp, _ := http.Post(rca.url+"/info", "application/json", bytes.NewBuffer(data))

	var ballotInfo types.Ballot
	utility.DecodeResponse(resp, &ballotInfo)
	switch resp.StatusCode {
	case 200:
		fmt.Println(ballotInfo)
	case 400:
		log.Println("Something went wrong with the request")
	case 501:
		log.Println("This ballot doesn't exist")
	default:
		log.Fatalln("Unknown status code returned by server")
	}
}

func (rca *RestClientAgent) changeId() {
	fmt.Println("Old ID: " + rca.id)
	fmt.Println("Enter your new ID:")
	fmt.Scan(&rca.id)
}

func (rca *RestClientAgent) shutDownServer() {
	resp, _ := http.Get(rca.url + "/shutdown")

	if resp == nil {
		log.Println("Server successfully shut down")
	} else {
		log.Fatal("There was an error while trying to shut down the server")
	}

}

func (rca *RestClientAgent) Run() {
	var clCmd string

	for {
		clCmd = mainMenu()
		fmt.Println("--------------------------------------------------")
		switch clCmd {
		case "0":
			rca.newBallot()
		case "1":
			rca.vote()
		case "2":
			rca.result()
		case "3":
			rca.info()
		case "4":
			rca.changeId()
		case "5":
			rca.shutDownServer()
		case "6":
			fmt.Println("Shutting down the client...")
			return
		default:
			panic("Unknown command returned from restclientagent.mainMenu")
		}
	}
}
