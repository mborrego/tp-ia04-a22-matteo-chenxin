package types

import (
	"time"
)

type BallotPattern struct {
	Rule     string    `json:"rule"`
	Deadline time.Time `json:"deadline"`
	VoterIds []string  `json:"voter-ids"`
	Alts     int       `json:"#alts"`
}

type Ballot struct {
	Pattern     BallotPattern `json:"ballot-pattern"`
	Profile     [][]int       `json:"profile"`
	Winner      int           `json:"winner"`
	Ranking     []int         `json:"ranking"`
	AgentDoneId []string      `json:"voted-id"`
}

type BallotId struct {
	BallotId string `json:"ballot-id"`
}

type Vote struct {
	AgentId string `json:"agent-id"`
	VoteId  string `json:"vote-id"`
	Prefs   []int  `json:"prefs"`
	Options []int  `json:"options"`
}

type Result struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}
