/*
-------------------------------------------------------------
Contains utility functions that can be called from anywhere


-------------------------------------------------------------
*/

package utility

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
)

// Checks if the slice of comparable elements "slc" contains the element "x"
func Contains[T comparable](slc []T, x T) bool {
	for _, v := range slc {
		if v == x {
			return true
		}
	}
	return false
}

// Returns the position of the element "x" in the slice of comparable elements "slc"
func Position[T comparable](slc []T, x T) int {
	for i, v := range slc {
		if v == x {
			return i
		}
	}
	return -1
}

// Decodes the request and stores the structure in str
func DecodeRequest[T any](r *http.Request, str *T) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	json.Unmarshal(buf.Bytes(), &str)
}

// Decodes the response and stores the structure in str
func DecodeResponse[T any](r *http.Response, str *T) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	json.Unmarshal(buf.Bytes(), &str)
}

func Rank(alt int, prefs []int) int {
	for i := range prefs {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

// Returns true only when alt1 is prefered to alt2
func IsPref(alt1, alt2 int, prefs []int) bool {
	var indice_alt1 int = Rank(alt1, prefs)
	var indice_alt2 int = Rank(alt2, prefs)
	if indice_alt1 < indice_alt2 {
		return true
	} else {
		return false
	}
}

func CheckProfile(prefs [][]int) error {
	length := len(prefs[0])
	for _, v := range prefs {
		if length != len(v) {
			return errors.New("la taille de préférence d'alternatives de chaque individus est différente")
		}
	}
	return nil
}

func MaxCount(count map[int]int) (bestAlts []int) {

	//Détermine la valeur maximum
	max := 0
	for _, v := range count {
		if v > max {
			max = v
		}
	}

	//Stocke les indices de la valeur maximum
	for i, v := range count {
		if v == max {
			bestAlts = append(bestAlts, i)
		}
	}

	return bestAlts
}

func RmvMissingPrefs(profile [][]int) [][]int {
	newProfile := make([][]int, 0)
	for _, v := range profile {
		if len(v) != 0 {
			newProfile = append(newProfile, v)
		}
	}
	return newProfile
}
