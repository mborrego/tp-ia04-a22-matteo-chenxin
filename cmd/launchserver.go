package main

import (
	"TP1/restserveragent"
	"fmt"
)

func main() {
	const urlserv = ":8080"

	fmt.Println("Setting up the server...")

	servAgt := restserveragent.NewRestServerAgent(urlserv)

	servAgt.Start()
}
