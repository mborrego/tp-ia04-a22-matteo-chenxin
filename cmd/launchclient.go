package main

import (
	"TP1/restclientagent"
	"fmt"
)

func main() {
	var clientId string = "example"
	const urlCl = "http://localhost:8080"

	fmt.Println("How would you like to call the client?")
	fmt.Scan(&clientId)
	
	fmt.Println("Launching the client example under the name \""+clientId+"\"...")
	clAgt := restclientagent.NewRestClientAgent(clientId, urlCl)

	clAgt.Run()
}
